import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Adatok szűrése
 * 
 * Készítsen programot, amely a szabványos bemenetről alkalmazottak vesszővel elválasztott adatait beolvasva kiszámolja
 * azok havi összbevételét (jutalékkal együtt), akik munkaköri beosztására illeszkedik a szintén beolvasott szöveg!
 * 
 * A bemenet specifikációja
 * 
 * A bemenet első sorában egy sztring, a második sorától az állományvégjelig alkalmazottak adatai (soronként pontosan egy) érkeznek.
 * Egyazon alkalmazott adatai egymástól vesszővel vannak elválasztva. Az első oszlopban az alkalmazott azonosítója, a másodikban a neve,
 * a harmadikban a havi fizetése (legfeljebb ötjegyű, százasokra kerekített pozitív egész), a negyedikben az általa kapott jutalék
 * százalékban kifejezve (0 és 100 közötti egész, de akár el is maradhat, ezt két egymást követő veszző jelzi), az ötödik oszlopban
 * pedig a beosztása helyezkedik el.
 * 
 * A kimenet specifikációja
 * 
 * A kimenet a szabványos kimenetre kerül és egyetlen sorból áll. Az első sorban beolvasott szövegre illeszkedő beosztású alkalmazottak
 * havi összfizetése kerül ki.
 * 
 * Példa bemenet
 * 
 * Sales
 * 145,John Russell,14000,40,Sales Manager
 * 148,Gerald Cambrault,11000,30,Sales Manager
 * 171,William Smith,7400,15,Sales Representative
 * 156,Janette King,10000,35,Sales Representative
 * 151,David Bernstein,9500,25,Sales Representative
 * 172,Elizabeth Bates,7300,15,Sales Representative
 * 131,James Marlow,2500,,Stock Clerk
 * 144,Peter Vargas,2500,0,Stock Clerk
 * 141,Trenna Rajs,3500,100,Stock Clerk
 * 134,Michael Rogers,2900,,Stock Clerk
 * 120,Matthew Weiss,8000,,Stock Manager
 * 
 * Példa kiment
 * 
 * 76180
 */
public class AdatokSzurese {
    public static void main(String[] args) throws FileNotFoundException {

        Scanner sc = new Scanner(new File("AdatokSzureseMinta.txt"));
        int id;
        String nev;
        int kereset;
        double bonusz;
        String beosztas;
        int osszeg = 0;
        String beosztasKategoria = sc.nextLine();

        while (sc.hasNextLine()) {
            Scanner sorScanner = new Scanner(sc.nextLine());
            sorScanner.useDelimiter(",");

            id = sorScanner.nextInt();
            nev = sorScanner.next();
            kereset = sorScanner.nextInt();
            bonusz = sorScanner.hasNextDouble() ? sorScanner.nextDouble() : -1;
            beosztas = sorScanner.next();

            if (beosztas.contains(beosztasKategoria)) {
                if (bonusz == -1) {
                    osszeg += kereset;
                } else {
                    osszeg += kereset + (kereset * bonusz) / 100;
                }
            }

        }
        System.out.println(osszeg);
    }
}
