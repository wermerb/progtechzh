import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Speciális megfordítás
 * 
 * Készítsen programot, amely meghatározza egy számsorozat speciális megfordítását!
 * Egész számok egy sorozatának (n, m) paraméterű speciális megfordítása alatt azt értjük, amikor a sorozat n-edik
 * elemétől kezdve az m-edik elemeket (vagyis a kiinduló sorozat (n + i * m)-edik elemeit, ahol i >= 0) tartalmazó részsorozatot
 * megfordítjuk úgy, hogy a többi elem változatlanul marad! Pl. az [1, 8, 7, 2, 9, 18, 12, 0] sorozat (2, 2) paraméterű speciális
 * megfordítása a [1, 0, 7, 18, 9, 2, 12, 8] sorozat, mivel a kiinduló sorozatban a második elemtől kezdve minden második elem alkotta
 * részsorozat a [8, 2, 18, 0] sorozat, amelynek megfordítása a [0, 18, 2, 8] sorozat.
 * Vegyük észre, hogy a kiinduló sorozat többi elemének sorrendje nem változik!
 * 
 * A bemenet specifikációja
 * 
 * A bemeneten állományvégjelig tetszőleges számú teszteset érkezik. Minden teszteset pontosan két sorból áll.
 * Egy teszteset első sorában két, egymástól szóközzel elválasztott egész szám áll (n és m), második sorában pedig egymástól vesszővel
 * és whitespace karakterekkel elválasztott, 10000-nél kisebb abszolút értékű egész számok legalább 1 és legfeljebb 1000 elemű sorozata.
 * 
 * A kimenet specifikációja
 * 
 * A program tesztesetenként pontosan egy sort írt a kimenetre, amely a tesztesetben megadott sorozat (n, m) paraméterű speciális
 * megfordítását tartalmazza. Az eredménysorozat bármely két elemét pontosan egy vessző és egy szóköz választja el egymástól.
 * (A sorozat első eleme előtt ill. utolsó eleme után nincs sem vessző, sem pedig szóköz!)
 * 
 * Példa bemenet
 * 2 2
 * 1, 8, 7, 2, 9, 18, 12, 0
 * 1 1
 * 1, 8, 7, 2, 9, 18, 12, 0, 5, 2, 3, 7, 4
 * 1 6
 * 1, 8, 7, 2, 9, 18, 12, 0, 5, 2
 * 3 10
 * 1, 8, 7, 2, 9, 18, 12, 0
 * 1 4
 * 1, 8, 7, 2, 9, 18, 12, 0, 5
 * 
 * Példa kimentre
 * 
 * 1, 0, 7, 18, 9, 2, 12, 8
 * 4, 7, 3, 2, 5, 0, 12, 18, 9, 2, 7, 8, 1
 * 12, 8, 7, 2, 9, 18, 1, 0, 5, 2
 * 1, 8, 7, 2, 9, 18, 12, 0
 * 5, 8, 7, 2, 9, 18, 12, 0, 1
 */
public class SpecialisForditas {

    public static void main(String[] args) throws FileNotFoundException {

        int sorozat[] = new int[1000];
        List<Integer> reszSorozat = new ArrayList<>();
        int sor = 1;
        int sorozatHossz;
        int hanyadikElemtol = 0;
        int elemGyakorisag = 0;

        Scanner sc = new Scanner(new File("SpecialisForditasMinta.txt"));
        while (sc.hasNextLine()) {
            Scanner sorScanner = new Scanner(sc.nextLine());
            if (sor % 2 != 0) {
                sor++;
                sorScanner.useDelimiter(" ");
                hanyadikElemtol = sorScanner.nextInt();
                elemGyakorisag = sorScanner.nextInt();
            } else {
                sor++;
                sorScanner.useDelimiter(", ");
                sorozatHossz = 0;

                while (sorScanner.hasNext()) {
                    sorozat[sorozatHossz] = sorScanner.nextInt();
                    sorozatHossz++;
                }

                for (int j = hanyadikElemtol - 1; j < sorozatHossz; j += elemGyakorisag) {
                    reszSorozat.add(sorozat[j]);
                }

                int z = hanyadikElemtol - 1;
                for (int j = reszSorozat.size() - 1; j > -1; j--, z += elemGyakorisag) {
                    sorozat[z] = reszSorozat.get(j);
                }

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < sorozatHossz; i++) {
                        sb.append(sorozat[i]).append(", ");
                }
                sb.delete(sb.length() - 2, sb.length() - 1);
                
                System.out.println(sb.toString());
            }
        }
    }
}
