import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Mátrix
 *
 * Írjon programot, amely meghatározza egy tetszőleges méretű,
 * abszolút értékben tízezernél kisebb egész értékeket tartalmazó mátrix olyan oszlopainak sorszámát,
 * amelyekben a legtöbb 0 elem van! Az első oszlop oszlopsorszáma 1, a másodiké 2, stb.
 *
 * A bemenet specifikációja
 *
 * A bemenet első sorában két egész érték (n és m), a mátrix sorainak (n), majd oszlopainak a száma (m),
 * egymástól pontosan egy szóközzel elválasztva szerepel. A bemenetnek pontosan n további sora lesz,
 * minden sor a mátrix egy-egy sorát írja le. Az egyes sorokban a mátrix értékei helyezkednek el,
 * minden elem pontosan 6 karakteren, jobbra igazítva.
 *
 * A kimenet specifikációja
 * A kimenet egyetlen sorból áll, amelyben egymástól pontosan egy szóközzel elválasztva az oszlopsorszámok helyezkednek el.
 * (Az első oszlop sorszáma 1, az utolsóé m.) Az első oszlopsorszám előtt, ill. az utolsó után nincs szóköz!
 *
 * Megoldás:
 * 1 3
 */
public class Matrix {

    public static void main(String[] args) throws IOException {

        Scanner sc = new Scanner(new File("Matrixminta.txt"));
        Scanner ssc = new Scanner(sc.nextLine());
        ssc.useDelimiter(" ");

        int sor = 0;
        int oszlop = ssc.nextInt();
        int matrix[][] = new int[oszlop][ssc.nextInt()];
        HashMap<Integer, Integer> oszlopEredmeny = new HashMap<>();
        //felépítjük a tényleges mátrixot
        while (sc.hasNextLine()) {
            Scanner sorScanner = new Scanner(sc.nextLine());
            sorScanner.useDelimiter(" ");
            oszlop = 0;
            while (sorScanner.hasNext()) {
                /*
                A Scanner-nek az üres Stringet állítottuk be delimiternek.
                A hasNextInt() metódus segítségével meg tudjuk nézni, hogy az aktuális elem
                az int-e, ha igen akkor az a mátrix egy eleme, ha nem akkor nézzük a következő elemet.
                */
                if (sorScanner.hasNextInt()) {
                    int eredmeny = sorScanner.nextInt();
                    matrix[sor][oszlop] = eredmeny;
                    //ha az éppen vizsgált elem 0 akkor rögzítjük, hogy találtunk még egy nullát az aktuális oszlopban
                    if (eredmeny == 0) {
                        if (oszlopEredmeny.containsKey(oszlop)){
                            oszlopEredmeny.put(oszlop+1, oszlopEredmeny.get(oszlop+1) + 1);
                        }else{
                            oszlopEredmeny.put(oszlop+1,1);
                        }
                    }
                    oszlop++;
                } else {
                    sorScanner.next();
                }
            }
            sor++;
        }

        //Elkészítjük az eredmény listánkat.
        int max = 0;
        List<Integer> eredmeny = new ArrayList<>();
        for (Map.Entry<Integer, Integer> ered : oszlopEredmeny.entrySet()) {
            if (ered.getValue() == max){
                eredmeny.add(ered.getKey());
            }
            if (ered.getValue() > max){
                eredmeny.clear();
                eredmeny.add(ered.getKey());
                max = ered.getValue();
            }
        }

        //Kiírjuk az eredményt.
        StringBuilder sb = new StringBuilder();
        for (Integer integer : eredmeny) {
            sb.append(integer).append(" ");
        }
        sb.deleteCharAt(sb.length() - 1);
        System.out.println(sb.toString());
    }
}