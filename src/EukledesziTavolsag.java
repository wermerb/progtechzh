import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Euklideszi-távolság
 * 
 * Készítsen programot, amely a szabványos bemenetről állományvégjelig soronként az n-dimenziós tér egy pontjának egész koordinátáit olvassa be.
 * A program határozza meg, hogy mi a leghosszabb Euklideszi értelemben vett távolság a megadott pontok valamelyikei között!
 * Az (x1, x2, ...,xn) és az (y1, y2, ...,yn) pontok közötti Euklideszi-távolságot az alábbi módon értelmezzük: EukledesziKeplet.png
 * 
 * Az egyszerűség - és a nagyobb pontosság - kedvéért számoljunk az Euklideszi távolság négyzetével!
 * 
 * A bemenet specifikációja
 * 
 * A bemeneten állományvégjelig az n-dimenziós tér egy-egy pontjának egész koordinátái helyezkednek el, egymástól szóközzel elválasztva.
 * Minden tesztesetben azonos számú, a teszteset első sorával megegyező számú komponens van jelen.
 * 
 * A kimenet specifikációja
 * 
 * A kimenet a szabványos kimenetre kerül és egyetlen sorból áll.
 * Az input bármely két pontja között értelmezhető leghosszabb Euklideszi-távolság négyzetét kell a kimenetre írni.
 * 
 * Példa bemenet
 * 
 * 1 2 3
 * 0 -2 15
 * 1 11 -1
 * 
 * Példa kiment
 * 
 * 426
 */
public class EukledesziTavolsag {
    public static void main(String[] args) throws FileNotFoundException {

        Scanner sc = new Scanner(new File("EukledesziTavolsagMinta.txt"));
        int sor = 1;
        int[] x = new int[3];
        int[] y = new int[3];
        int elem;

        while (sc.hasNextLine()) {
            Scanner sorScanner = new Scanner(sc.nextLine());
            sorScanner.useDelimiter(" ");
            elem = 0;
            while (sorScanner.hasNext()) {
                if (sor % 2 != 0) {
                    x[elem] = sorScanner.nextInt();
                } else {
                    y[elem] = sorScanner.nextInt();
                }
                elem++;
            }
            sor++;
            if (sor == 5) {
                break;
            }
        }

        int osszeg = 0;
        for (int i = 0; i < 3; i++) {
            osszeg += Math.pow(Math.abs(x[i] - y[i]), 2);
        }
        
        System.out.println(Math.pow(Math.pow(osszeg,0.5),2));
    }
}
