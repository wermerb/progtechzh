import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Armstrong-számok
 * 
 * Egy n jegyű egész számot Armstrong-számnak nevezünk, ha minden számjegyét az n-edik hatványra emelve és összeadva,
 * az eredeti számot kapjuk. Armstrong szám az összes egyjegyű szám, de ilyen például a 153 vagy a 9474 is
 * (1^3 + 5^3 +3^3 = 1 + 125 + 27 = 153, ill. 9^4 + 4^4 + 7^4 + 4^4 = 6561 + 256 + 2401 + 256 = 9474).
 * Készítsen programot, amely meghatározza, hogy egy adott zárt intervallumban hány Armstrong-szám helyezkedik el!
 * 
 * A bemenet specifikációja
 * 
 * A bemeneten állományvégjelig tetszőleges számú teszteset érkezik. Minden teszteset pontosan egy sorból áll.
 * Egy teszteset két nemnegatív egész számot (a-t és b-t, ahol feltételezzük, hogy a <= b)) tartalmaz,
 * amelyeket egymástól pontosan egy szóköz választ el.
 * 
 * A kimenet specifikációja
 * 
 * A kimeneten minden tesztesethez tartozóan pontosan egy sor jelenik meg, amelyben egyetlen egész szám,
 * a tesztesetben kapott két szám által határolt zárt intervallumban (vagyis az [a,b]) található Armstong-számok darabszáma kap helyet.
 * 
 * Példa bemenet
 * 
 * 0 0
 * 10 10
 * 3 15
 * 310 410
 * 
 * Példa kimenetre
 * 
 * 1
 * 0
 * 7
 * 3
 */
public class AmstrongSzamok {
    public static void main(String[] args) throws FileNotFoundException {

        int talalt;
        int alsokorlat;
        int felsokorlat;
        int osszeg;
        Scanner sc = new Scanner(new File("AmstrongSzamokMinta.txt"));

        while (sc.hasNextLine()) {
            Scanner sorScanner = new Scanner(sc.nextLine());
            sorScanner.useDelimiter(" ");

            alsokorlat = sorScanner.nextInt();
            felsokorlat = sorScanner.nextInt();
            talalt = 0;
            for (int i = alsokorlat; i <= felsokorlat; i++) {
                char[] szamok = String.valueOf(i).toCharArray();
                osszeg = 0;
                for (char szam : szamok) {
                    osszeg += Math.pow(Character.getNumericValue(szam), szamok.length);
                }
                if (osszeg == i) {
                    talalt++;
                }
            }
            System.out.println(talalt);

        }
    }
}
